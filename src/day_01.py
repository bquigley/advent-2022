"""Hello world!"""
from textwrap import dedent


sample_elf_burdens_array = dedent(
    """1000
    2000
    3000

    4000

    5000
    6000

    7000
    8000
    9000

    10000"""
)


def elf_with_most_calories(elf_burdens_array):
    elf_burdens_strs = elf_burdens_array.split("\n\n")
    elf_burdens = (
        map(int, filter(None, string.split("\n"))) for string in elf_burdens_strs
    )
    sums = (sum(burden) for burden in elf_burdens)
    return max(sums)


def elves_with_most_calories(elf_burdens_array, number_of_elves=3):
    elf_burdens_strs = elf_burdens_array.split("\n\n")
    elf_burdens = (
        tuple(map(int, filter(None, string.split("\n")))) for string in elf_burdens_strs
    )
    sums = (sum(burden) for burden in sorted(elf_burdens))
    return sum(sorted(sums, reverse=True)[:number_of_elves])


def main():
    with open("input/day_01.txt") as infile:
        puzzle_input = infile.read()
        print(elves_with_most_calories(puzzle_input))


main()
