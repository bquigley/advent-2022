from textwrap import dedent
from typing import Union


class Rock:
    """Rock."""

    value = 0

    def __repr__(self):
        return "-rock-"


class Paper:
    """Paper."""

    value = 1

    def __repr__(self):
        return "-paper-"


class Scissors:
    """Scissors."""

    value = 2

    def __repr__(self):
        return "-scissors-"


values_map = {klass.value: klass for klass in (Rock, Paper, Scissors)}


Item = Union[Rock, Paper, Scissors]


def from_glyph(glyph: Union[str, Item]) -> Item:
    if glyph in ("A", "X"):
        return Rock()
    if glyph in ("B", "Y"):
        return Paper()
    if glyph in ("C", "Z"):
        return Scissors()
    return glyph


def fight(round: tuple[Item, Item]) -> int:
    return (round[1].value - round[0].value) % 3


assert fight((Rock, Rock)) == 0
assert fight((Scissors, Rock)) == 1


def wins(round: tuple[Item, Item]):
    return fight(round) == 1


assert not wins((Rock, Scissors))


def ties(round: tuple[Item, Item]):
    return fight(round) == 0


def loses(round: tuple[Item, Item]):
    return fight(round) == 2


assert loses((Rock, Scissors))

assert ties((Rock, Rock))
assert ties((Rock, Rock))


def score_round(round: tuple[Item, Item]) -> int:
    instantiated_round = tuple(map(from_glyph, round))
    score_from_result = (
        6 if wins(instantiated_round) else 3 if ties(instantiated_round) else 0
    )
    return score_from_result + instantiated_round[1].value + 1


def parse_strings(text) -> tuple[tuple[Item, ...], ...]:
    return tuple(
        filter(
            None,
            (tuple(line.split()) for line in text.split("\n")),
        )
    )


def count_score_from_array(games: list[tuple[Item, Item]]):
    return sum((score_round(row) for row in games))


def update_data_with_corrected_understanding_of_strategy(data):
    recommendation_map = {
        "X": 2,  # Loss
        "Y": 0,  # Tie
        "Z": 1,  # Win
    }
    for record in data:
        recommendation = record[1]
        record = tuple(map(from_glyph, record))
        while not fight(record) == recommendation_map[recommendation]:
            record = (record[0], values_map[(record[1].value - 1) % 3]())
        yield record


sample_input = dedent(
    """A Y
    B X
    C Z"""
)


data = parse_strings(open("../input/day_02.txt").read())
print(f"Read {len(data)} lines.")
print("Top:", data[:3])
print("Tail:", data[-3:])
print(
    count_score_from_array(update_data_with_corrected_understanding_of_strategy(data))
)
