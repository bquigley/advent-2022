import math
from dataclasses import dataclass

SAMPLE = """vJrwpWtwJgWrhcsFMMfFFhFp
jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL
PmmdzqPrVvPwwTWBwg
wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn
ttgJtRGJQctTZtZT
CrZsJsPPZsGzwwsLwLmpwMDw"""


@dataclass
class Rucksack:
    burden: str

    @property
    def compartments(self) -> tuple[str, str]:
        midpoint = math.ceil(len(self.burden) / 2)
        return (self.burden[:midpoint], self.burden[midpoint:])

    @classmethod
    def from_text_blob(cls, text: str) -> tuple["Rucksack", ...]:
        return tuple(cls(line) for line in text.split("\n") if line)

    def char_in_common_between_two_compartments(self):
        return set(self.compartments[0]).intersection(set(self.compartments[1])).pop()

    @staticmethod
    def priority_from_glyph(glyph):
        ordinal = ord(glyph)
        if ordinal in range(ord("a"), ord("z") + 1):
            return ordinal - 96
        elif ordinal in range(ord("A"), ord("Z") + 1):
            return ordinal - 38


@dataclass
class ElfGroup:
    elves: tuple[Rucksack, Rucksack, Rucksack]

    @classmethod
    def from_text_blob(cls, text: str, group_size=3) -> tuple["ElfGroup", ...]:
        sacks = Rucksack.from_text_blob(text)
        rval = []
        buffer = []
        for i, elf_sack in enumerate(sacks):
            buffer.append(elf_sack)
            if len(buffer) == group_size or i == len(sacks) - 1:
                rval.append(buffer.copy())
                buffer = []
        return tuple(cls(elves=tuple(group)) for group in rval)

    def char_in_common_amongst_group(self):
        intersection = None
        for elf in self.elves:
            if intersection is None:
                intersection = set(elf.burden)
            else:
                intersection = intersection.intersection(set(elf.burden))
        return intersection.pop()


for sack in Rucksack.from_text_blob(SAMPLE):
    assert len(sack.compartments[0]) == len(sack.compartments[1])
assert Rucksack("abcde").compartments == ("abc", "de")
assert Rucksack.priority_from_glyph("z") == 26
assert Rucksack.priority_from_glyph("Z") == 52
test_sacks = ElfGroup.from_text_blob(SAMPLE)
assert len(test_sacks) == 2
assert [len(sack.elves) for sack in test_sacks] == [3, 3]


with open("../input/day_03.txt") as infile:
    elf_groups = ElfGroup.from_text_blob(infile.read())
print(
    sum(
        (
            Rucksack.priority_from_glyph(group.char_in_common_amongst_group())
            for group in elf_groups
        )
    )
)
