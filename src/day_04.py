#!/usr/bin/env python3

SAMPLE_INPUT = """2-4,6-8
2-3,4-5
5-7,7-9
2-8,3-7
6-6,4-6
2-6,4-8"""


def glyph_to_range(glyph):
    (start, finish) = glyph.split("-")
    return range(int(start), int(finish) + 1)


assert glyph_to_range("2-4") == range(2, 5)


def parse_text_to_ranges(text):
    glyphs = (line.split(",") for line in text.split("\n"))
    for cells in glyphs:
        if cells and cells[0]:
            yield tuple(map(glyph_to_range, cells))


print(list(parse_text_to_ranges(SAMPLE_INPUT)))


def range_a_contains_range_b(range_a, range_b):
    return range_b.start >= range_a.start and range_b.stop <= range_a.stop


def overlaps_entirely(range_a, range_b):
    return range_a_contains_range_b(range_a, range_b) or range_a_contains_range_b(
        range_b, range_a
    )


assert overlaps_entirely(range(2, 9), range(3, 8))
assert overlaps_entirely(range(3, 8), range(2, 9))
sample_entire_overlaps = [
    range_pair
    for range_pair in parse_text_to_ranges(SAMPLE_INPUT)
    if overlaps_entirely(*range_pair)
]
assert len(sample_entire_overlaps) == 2


def range_a_contains_range_b_start(range_a, range_b):
    return range_a.start <= range_b.start and range_a.stop > range_b.start


def overlaps_at_all(range_a, range_b):
    return range_a_contains_range_b_start(
        range_a, range_b
    ) or range_a_contains_range_b_start(range_b, range_a)


sample_partial_overlaps = [
    range_pair
    for range_pair in parse_text_to_ranges(SAMPLE_INPUT)
    if overlaps_at_all(*range_pair)
]
assert len(sample_partial_overlaps) == 4
with open("../input/day_04.txt") as infile:
    parsed_input = parse_text_to_ranges(infile.read())
overlaps = [range_pair for range_pair in parsed_input if overlaps_at_all(*range_pair)]
print(len(overlaps))
