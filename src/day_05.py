#!/usr/bin/env python3

SAMPLE_INPUT = """    [D]
[N] [C]
[Z] [M] [P]
 1   2   3

move 1 from 2 to 1
move 3 from 1 to 3
move 2 from 2 to 1
move 1 from 1 to 2
"""


class CraneOperation:
    @staticmethod
    def process_crate_text(crate_text):
        crate_rows = crate_text.split("\n")
        footer = crate_rows[-1]
        crate_stack_indexes = [i for i, char in enumerate(footer) if char.isnumeric()]
        crate_grid = [[] for _ in range(len(crate_stack_indexes))]
        for crate_row in crate_rows[:-1]:  # All but the last
            for crate_num, crate_stack_index in enumerate(crate_stack_indexes):
                if crate_stack_index >= len(crate_row):
                    continue
                character_at_index = crate_row[crate_stack_index]
                if character_at_index.isalpha():
                    crate_grid[crate_num] = [character_at_index] + crate_grid[crate_num]
        return crate_grid

    @staticmethod
    def parse_instruction(instruction_text):
        number, location, destination = tuple(
            map(
                int,
                (string for string in instruction_text.split() if string.isnumeric()),
            )
        )
        # Account for our 0-indexed array:
        location -= 1
        destination -= 1
        return number, location, destination

    def process_instruction(self, instruction_text):
        (number_to_move, location, destination) = self.parse_instruction(
            instruction_text
        )
        for _ in range(number_to_move):
            self.crate_grid[destination].append(self.crate_grid[location].pop())

    def process_instruction_while_retaining_order(self, instruction_text):
        (number_to_move, location, destination) = self.parse_instruction(
            instruction_text
        )
        start_moving_index = (
            len(self.crate_grid[location]) - number_to_move
        )  # e.g. 2 crates there, move 1, so start moving at index 1
        for _ in range(number_to_move):
            self.crate_grid[destination].append(
                self.crate_grid[location].pop(start_moving_index)
            )

    def report(self):
        print("".join([column[-1] for column in self.crate_grid]))

    def __init__(self, text=SAMPLE_INPUT):
        crate_text, instructions = text.split("\n\n")
        self.crate_grid = self.process_crate_text(crate_text)
        self.instructions = [
            self.process_instruction_while_retaining_order(instruction)
            for instruction in instructions.split("\n")
            if instruction
        ]
        self.report()


with open("../input/day_05.txt") as infile:
    sample_crane = CraneOperation(infile.read().strip())
