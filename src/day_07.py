#!/usr/bin/env python3
from dataclasses import dataclass
from typing import Union

SAMPLE_INPUT = """$ cd /
$ ls
dir a
14848514 b.txt
8504156 c.dat
dir d
$ cd a
$ ls
dir e
29116 f
2557 g
62596 h.lst
$ cd e
$ ls
584 i
$ cd ..
$ cd ..
$ cd d
$ ls
4060174 j
8033020 d.log
5626152 d.ext
7214296 k
"""


@dataclass
class File:
    size: int


@dataclass
class Directory:
    children: dict[str, Union["Directory", File]]
    _total_size: int = None

    def get_or_create_child_dir(
        self, path: tuple[str, ...]
    ) -> Union["Directory", File]:
        directory = self
        for name in path:
            if name not in directory.children:
                directory.children[name] = directory.__class__({})  # Directory()
            if isinstance(directory, File):
                raise ValueError("Bad path.")
            directory = directory.children[name]
        return directory

    @property
    def total_size(self):
        if self._total_size is None:
            self._total_size = sum(
                (
                    item.size if isinstance(item, File) else item.total_size()
                    for item in self.children.values()
                )
            )
        return self._total_size


def parse_files(commands):
    filesystem = Directory({})
    path = ()
    working_directory = None
    total_size = 0

    for command in commands.split("\n"):
        # TODO this is working, except that it says "for directories over 1000 b in size" or something
        print(f"'{command}'")
        words = command.split()
        if not command:
            continue
        if command.startswith("$"):
            # Parse a command.
            if words[1] == "cd":
                # Process that we are entering a new directory.
                if words[-1] == "..":
                    # Go up a directory.
                    # Report working directory size before leaving it:
                    if working_directory:
                        total_size += working_directory.total_size
                        print(f"{'/'.join(path)}: {working_directory.total_size}")
                    path = path[:-1]
                else:
                    path += (words[-1],)
                print("Entering", path)
            elif words[1] == "ls":
                # Parse listing of files in a directory.
                # We actually don't do anything with this; we'll read the output
                # down below. But make sure it is that simple...
                if len(words) > 2:
                    raise ValueError("Args to `ls` are not implemented yet.")
                else:
                    pass
        else:
            # Parse output from the machine.
            if words[0] == "dir":
                pass  # we could get or create but it's redundant I guess
            elif not words[0].isnumeric():
                raise Exception(f"Couldn't parse {command}.")
            else:
                filesize = int(words[0])
                filename = words[1]
                working_directory = filesystem.get_or_create_child_dir(path)
                working_directory.children[filename] = File(size=filesize)
    print(f"{'/'.join(path)}: {working_directory.total_size}")
    print("Total size:", total_size)
    return filesystem


INPUT = open("../input/day_07.txt").read()
INPUT = SAMPLE_INPUT
filesystem = parse_files(INPUT)
