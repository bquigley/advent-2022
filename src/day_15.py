from dataclasses import dataclass
from typing import Optional

SAMPLE_INPUT = """Sensor at x=2, y=18: closest beacon is at x=-2, y=15
Sensor at x=9, y=16: closest beacon is at x=10, y=16
Sensor at x=13, y=2: closest beacon is at x=15, y=3
Sensor at x=12, y=14: closest beacon is at x=10, y=16
Sensor at x=10, y=20: closest beacon is at x=10, y=16
Sensor at x=14, y=17: closest beacon is at x=10, y=16
Sensor at x=8, y=7: closest beacon is at x=2, y=10
Sensor at x=2, y=0: closest beacon is at x=2, y=10
Sensor at x=0, y=11: closest beacon is at x=2, y=10
Sensor at x=20, y=14: closest beacon is at x=25, y=17
Sensor at x=17, y=20: closest beacon is at x=21, y=22
Sensor at x=16, y=7: closest beacon is at x=15, y=3
Sensor at x=14, y=3: closest beacon is at x=15, y=3
Sensor at x=20, y=1: closest beacon is at x=15, y=3
"""


def th(number):
    """Thousands-separated format."""
    return f"{number:,}"


@dataclass
class Position:
    x_coordinate: int
    y_coordinate: int

    def __str__(self):
        return f"({th(self.x_coordinate)}, {th(self.y_coordinate)})"

    def copy(self):
        return Position(x_coordinate=self.x_coordinate, y_coordinate=self.y_coordinate)

    def distance_vertical_to_point(self, position: "Position"):
        return abs(position.y_coordinate - self.y_coordinate)

    def distance_horizontal_to_point(self, position: "Position"):
        return abs(position.x_coordinate - self.x_coordinate)

    def distance_to_point(self, position: "Position"):
        """The problem specifies we use the Manhattan Distance.

        ref: https://en.wikipedia.org/wiki/Taxicab_geometry
        """
        return self.distance_horizontal_to_point(
            position
        ) + self.distance_vertical_to_point(position)


Shadow = tuple[Position, Position, Position, Position]


@dataclass
class Sensor:
    location: Position
    closest_beacon: Position
    _distance_to_closest_beacon: Optional[int] = None
    _shadow: Optional[Shadow] = None

    @property
    def distance_to_closest_beacon(self):
        if self._distance_to_closest_beacon is None:
            self._distance_to_closest_beacon = self.location.distance_to_point(
                self.closest_beacon
            )
        return self._distance_to_closest_beacon

    @classmethod
    def from_string(cls, string):
        (loc_x, loc_y, beacon_x, beacon_y) = [
            int(item.split("=")[1].rstrip(",:"))
            for item in string.split()
            if "=" in item
        ]
        return cls(
            location=Position(loc_x, loc_y),
            closest_beacon=Position(beacon_x, beacon_y),
        )

    def rules_out_any_beacon_at_point(self, point: Position):
        """Knowing the closest beacon, we can rule out the existence of a beacon at a point that is closer."""
        return self.distance_to_closest_beacon >= self.location.distance_to_point(point)

    def last_point_covered_for_row(self, row_y_loc):
        """Jump to the rightmost point on a given row of this sensor's "shadow"."""
        # Find the rightmost hypothetical beacon that this sensor rules out, then move 1 to the right.
        y_distance_from_row = abs(self.location.y_coordinate - row_y_loc)
        x_distance_from_row = self.distance_to_closest_beacon - y_distance_from_row
        x_coord = self.location.x_coordinate + x_distance_from_row
        return Position(x_coordinate=x_coord, y_coordinate=row_y_loc)

    @property
    def shadow(self) -> tuple[Position, Position, Position, Position]:
        if not self._shadow:
            self._shadow = tuple(
                Position(
                    self.location.x_coordinate
                    + (self.distance_to_closest_beacon * vertex_factor[0]),
                    self.location.y_coordinate
                    + (self.distance_to_closest_beacon * vertex_factor[1]),
                )
                for vertex_factor in (
                    (-1, 0),
                    (1, 0),
                    (0, -1),
                    (0, 1),
                )
            )
        return self._shadow


test_sensor = Sensor(location=Position(0, 0), closest_beacon=Position(1, 1))
assert test_sensor.rules_out_any_beacon_at_point(Position(2, 0))
assert test_sensor.last_point_covered_for_row(0) == Position(2, 0)


@dataclass
class Grid:
    sensors: tuple[Sensor, ...] = ()
    location: Position = None
    distant_corner: Position = None

    def update_grid_size_for_sensor(self, sensor):
        # Initialize
        if self.location is None:
            self.location = sensor.location.copy()
            self.distant_corner = sensor.location.copy()
        # For each of the sensor's four vertices, update our location:
        for vertex in sensor.shadow:
            # If this vertex is located left or north of us, update our location;
            # if it is located right or south of us, update our distant_corner.
            if self.location.x_coordinate > vertex.x_coordinate:
                self.location.x_coordinate = vertex.x_coordinate
            elif self.distant_corner.x_coordinate < vertex.x_coordinate:
                self.distant_corner.x_coordinate = vertex.x_coordinate
            if self.location.y_coordinate > vertex.y_coordinate:
                self.location.y_coordinate = vertex.y_coordinate
            elif self.distant_corner.y_coordinate < vertex.y_coordinate:
                self.distant_corner.y_coordinate = vertex.y_coordinate

    @classmethod
    def from_text(cls, text):
        grid = cls()
        for line in filter(None, text.split("\n")):
            sensor = Sensor.from_string(line)
            grid.update_grid_size_for_sensor(sensor)
            grid.sensors += (sensor,)
        return grid

    def count_beacons_in_row(self, row_num):
        return len(
            {
                (
                    sensor.closest_beacon.x_coordinate,
                    sensor.closest_beacon.y_coordinate,
                )
                for sensor in self.sensors
                if sensor.closest_beacon.y_coordinate == row_num
            }
        )

    def count_candidate_positions_for_row(self, row_y_loc):
        ruled_out = 0
        position = Position(
            x_coordinate=self.location.x_coordinate,  # < our relative 0
            y_coordinate=row_y_loc,
        )
        while position.x_coordinate < self.distant_corner.x_coordinate + 2:
            print("Solving", position)

            # Rule out any locations where there is in fact a beacon?
            for sensor in self.sensors:
                distance_to_sensor = position.distance_to_point(sensor.location)
                if sensor.rules_out_any_beacon_at_point(position):
                    print(
                        f"Nothing here - sensor {th(distance_to_sensor)} squares away at {sensor.location} has a beacon "
                        f"{th(sensor.distance_to_closest_beacon)} squares away at "
                        f"{sensor.closest_beacon}, ruling out one here."
                    )
                    ruled_out += 1
                    # Jump across all space covered_by_beacon
                    x_coord_to_jump_to = sensor.last_point_covered_for_row(row_y_loc)
                    squares_jumping = (
                        x_coord_to_jump_to.x_coordinate - position.x_coordinate
                    )
                    print("Jumping", th(squares_jumping), "Squares.")
                    ruled_out += squares_jumping
                    position = x_coord_to_jump_to
                    break
            position.x_coordinate += 1

        # One more thing - we're counting spaces where there can be no beacon,
        # so we have to go back through these shadows and make sure to omit the
        # ones where there is in fact a beacon.
        ruled_out -= self.count_beacons_in_row(row_y_loc)
        return ruled_out

    def report(self, target_row):
        print(
            f"{th(self.count_candidate_positions_for_row(target_row))} spaces ruled out from row "
            f"{target_row} ({th(self.distant_corner.x_coordinate - self.location.x_coordinate)} spaces across.)"
        )


def main(test=True):
    text = SAMPLE_INPUT if test else open("../input/day_15.txt").read()
    grid = Grid.from_text(text)
    target_row = 10 if test else 2000000
    grid.report(target_row)


main(test=True)
