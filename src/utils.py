#!/usr/bin/env python3
from typing import Any, Iterable


def chained_getattr(
    item: Any, attrs: Iterable[str], default: Any = None, go_in_dicts: bool = True
):
    """Take a sequence of attr names and iteratively get them from `item`.

    item: Item to look for chained attrs in.
    attrs: Iterable of attribute names.
    default: Value to return if any is not found.
    go_in_dicts: If true, also check if items are dicts containing the attrs.
    """
    if not attrs:
        raise ValueError
    for attr in attrs:
        if go_in_dicts and isinstance(item, dict) and attr in item:
            item = item[attr]
        elif hasattr(item, attr):
            item = getattr(item, attr)
        else:
            return default
    return item
